﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WaypointPatrol : MonoBehaviour
{

    
    public NavMeshAgent navMeshAgent;

    public GameObject player;

    public void Start()
    {
        print("Start");
        player = GameObject.FindGameObjectWithTag("Player");//Locates John Lemon
    }

    public void Update()
    {
        navMeshAgent.SetDestination(player.transform.position);//Follows John Lemon consistently 
    }

}